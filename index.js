const http = require('http');
const fs = require('fs');
const url = require('url');

module.exports = () => {
    const port = 8080;
    const logsPath = './logs.json';

    try {
        fs.mkdirSync('./files');
    }
    catch(e) {
        console.log(e);
    }

    function writeLog(entry, cb) {
        fs.readFile(logsPath, (err, data) => {
            let logs = [];

            if (err) {
                console.log(err);
            }
            else {
                try {
                    logs = JSON.parse(data).logs || [];
                } 
                catch(e) {
                    console.log(e);
                    logs = [];
                }
                
            }

            logs.push({
                message: entry,
                time: Date.now(),
            });

            fs.writeFile(logsPath, JSON.stringify({ logs: logs }, null, '\t'), (err) => {
                if (err) {
                    console.log(err);
                }

                cb();
            });
        });
    }

    http.createServer(function (req, res) {   
        const { pathname,  query } = url.parse(req.url, true);
        const method = req.method;
        const [ path, param ] = pathname.slice(1).split('/');
        
        switch(path) {
            case 'file': {
                if (method === 'POST') {
                    const filename = query.filename;
                    const content = query.content || '';

                    if (!filename) {
                        res.writeHead(400, {'Content-Type': 'text/html'});   
                        res.end('Filename not specified'); 
                        return;
                    }

                    fs.writeFile(`./files/${filename}`, content, 'utf8', (err) => {
                        if (err) {
                            console.log(err);
                            
                            res.writeHead(500, {'Content-Type': 'text/html'});   
                            res.end('Server error'); 
                            return;
                        }

                        writeLog(`New file with name '${filename}' saved`, () => {
                            res.writeHead(200, {'Content-Type': 'text/html'});   
                            res.end('Success!'); 
                        });
                    });
                }
                else if (method === 'GET') {
                    const filename = param;

                    if (!filename) {
                        res.writeHead(400, {'Content-Type': 'text/html'});   
                        res.end('Filename not specified'); 
                        return;
                    }

                    fs.readFile(`./files/${filename}`, 'utf8', (err, data) => {
                        if (err) {
                            console.log(err);
                            
                            res.writeHead(400, {'Content-Type': 'text/html'});   
                            res.end(`File with name ${filename} not found`); 
                            return;
                        }

                        writeLog(`File with name '${filename}' was read`, () => {
                            res.writeHead(200, {'Content-Type': 'text/html'});   
                            res.end(data); 
                        });
                    });
                }
                else {
                    res.writeHead(404, {'Content-Type': 'text/html'});   
                    res.end('Resource not found'); 
                }
                break;
            }
            case 'logs': {
                if (method === 'GET') {
                    fs.readFile(logsPath, (err, data) => {
                        if (err) {
                            res.writeHead(500, {'Content-Type': 'application/json'});   
                            res.end('Logs file not found'); 
                        }
                        else {
                            let logs = [];

                            try {
                                logs = JSON.parse(data).logs || [];
                            }
                            catch(e) {
                                console.log(e);
                                logs = [];
                            }

                            const from = query.from ? parseInt(query.from) : 0;
                            const to = query.to ? parseInt(query.to) : 0;

                            const filteredLogs = logs.filter(log => (!from || log.time >= from) && (!to || log.time <= to));


                            writeLog(`Requested logs${query.from ? ` from ${from}` : ''}${query.to ? ` to ${to}` : ''}`, () => {
                                res.writeHead(200, {'Content-Type': 'application/json'});   
                                res.end(JSON.stringify({logs: filteredLogs})); 
                            });
                        }
                    });
                }
                else {
                    res.writeHead(404, {'Content-Type': 'text/html'});   
                    res.end('Resource not found'); 
                }
                break;
            }
            default: {
                res.writeHead(404, {'Content-Type': 'text/html'});   
                res.end('Resource not found'); 
                break;
            }
        }
    }).listen(port, () => {
        console.log(`Server is listening on port ${port}`);
    });
}